- we are the virus → complete information
- the boids only see symptoms
- symptoms develop with a certain probability
- probablity that they see the symptoms increases with proximity
- probability that they catch the disease from an infectious boid increases with exposure time
- after its infection a boid can either die or recover, when it dies it's just removed from the game & death count is increased by 1

# experiment

- one group of naive boids who do not avoid sick boids
- one group of social distancing boids
- show realtime stats for both and see which group does better
