# parameters to test:
- MAX_INFECTIOUS_PROBABILITY: from 0.1 to 1, by 0.2
- SOCIAL_DISTANCE: from 0 to 30 by 5
- INFECTIOUS_PERIOD_PARAMS: from 1 to 10, step_size 2

# plot:
- max_r0
- outbreak_duration
- number of susceptible boids

# Workflow

1. Run the experiment manually with minimums and maximums (see above) to verify meaningful data
2. Make a script to execute this from the command line (with commented out display updates) 
1. Set hyper parameter value
2. Set STATE_COUNT_COLLECTION_ONCE_PER_CYCLE to `False`
3. Make screenrecording
4. Speed up recording by 2x
5. Set STATE_COUNT_COLLECTION_ONCE_PER_CYCLE to `True`
6. Collect data for 5 runs or so
7. Take averages of max_r0 and outbreak_duration
8. Feed averages to plot

