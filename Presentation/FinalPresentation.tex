% to compile: cd Presentation
% pdflatex FinalPresentation.tex;bibtex FinalPresentation.aux;pdflatex FinalPresentation.tex;pdflatex FinalPresentation.tex
\documentclass[c,dvipsnames]{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{pgfplots}
\usepackage{color}
\usepackage{xcolor}
\usepackage{url}
\usepackage{textcomp}
\usepackage{listings}
\usepackage{parskip}
\usepackage{multicol}
\usepackage[square]{natbib}
\usepackage{multimedia}
\usepackage{tikz}
\usetikzlibrary{arrows,automata,positioning,fillbetween}
\tikzstyle{label} = [text width=5em, text centered]

\definecolor{purple}{HTML}{8e03a3}
\definecolor{blue}{HTML}{0369a3}
\definecolor{yellow}{HTML}{e9b913}
\definecolor{red}{HTML}{ff0000}
\definecolor{green}{HTML}{18a303}
\definecolor{grey}{HTML}{9f9f9f}

% \pgfplotsset{compat=1.12}
\pgfplotsset{
/pgfplots/area cycle list/.style={
    /pgfplots/cycle list={%
    {purple!70!black,fill=purple,mark=none},%
    {blue!70!black,fill=blue,mark=none},%
    {yellow!70!black,fill=yellow,mark=none},%
    {red!70!black,fill=red,mark=none},
    {green!70!black,fill=green,mark=none},
    {grey!70!black,fill=grey,mark=none},
    }
},
}

\usetheme{Amsterdam}

\addtobeamertemplate{navigation symbols}{}{%
    \usebeamerfont{footline}%
    \usebeamercolor[fg]{footline}%
    \hspace{1em}%
    \raisebox{1.2pt}[0pt][0pt]{\insertframenumber/\inserttotalframenumber}
}

\setbeamertemplate{caption}[numbered]
\setbeamertemplate{bibliography item}[text]

\title{BoidFlu}
\subtitle{Final Presentation ATAI}
\author{William Stock and Uta Lemke}
\date{\today}

\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\begin{frame}
	\tableofcontents
\end{frame}

\section{Project Introduction}
\subsection{Topic Introduction}
\begin{frame}{Boids}
\begin{quote}
   ``[A] computer model of coordinated animal motion, such as bird flocks and fish schools'' \flushright \citep{boids-website_reynolds}
\end{quote}
\pause
\begin{block}{The model consists of}\pause
\begin{itemize}
    \item \textbf{Boids:} basic actors \pause
    \item \textbf{3 main rules:}\pause
    \begin{enumerate}
        \item Separation\pause
        \item Alignment\pause
        \item Cohesion
    \end{enumerate}
\end{itemize}
\end{block}
\end{frame}

\subsection{Motivation \& Goals} % what problem are we trying to solve

\begin{frame}{Motivation and Goals}
\begin{block}{Motivation}\pause
The ongoing pandemic has raised interest in epidemiology.  We wondered if it were possible to gain insight into the effectiveness of simple counter-measures by simulating them with the boids model. 
\end{block}\pause
\begin{block}{Goals}\pause % what will we do
    \begin{itemize}
        \item develop a fully functioning boid simulation \pause
        \item combine rules of epidemiology with the base model \pause
        \item test variations of epidemiological variables and behaviour 
    \end{itemize}
\end{block}
\end{frame}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\section{Research}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\begin{frame}{Craig Reynold's initial paper about boids \citep{original-boids-paper}}
\begin{block}{His discovery}\pause
\begin{itemize}
    \item the original 1986 model showed that simple rules could model flocking behavior\pause
    \item in contrast to the prevailing implementations at the time which were more about pre-scripted paths
\end{itemize}
\end{block}\pause
%~~~~~~~~~~~~~
\begin{block}{His approach}\pause
\begin{itemize}
    \item assumption that a flock is simply the result of the interaction between the behaviors of individual birds\pause
    \item to simulate a flock we simulate the behavior of an individual bird\pause % (or at least that portion of the bird's behavior that allows it to participate in a flock)
    \item also simulate portions of the bird's perceptual mechanisms and aspects of the physics of aerodynamic flight % we will focus especially on the perception part of the equation, meaning that each boid has only a limited field of view to see the stuff it reacts to, e.g. sick boids. the whole picture is only visible by us, not by the individual actors
\end{itemize}
\end{block}
\end{frame}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\begin{frame}{SIR Model by Smith and Moore \footnotesize\citep{sir-model}}
\begin{figure}
    \centering
    \begin{tikzpicture}[shorten >=1pt,node distance=4cm,auto,scale=0.8, every node/.style={transform shape}]
  \tikzstyle{every state}=[fill={rgb:black,1;white,10}]

  \node[state,initial]   (s)  {$\textbf{s}usceptible$};
  \node[state] (i) [right of=s]  {$\textbf{i}nfectious$};
  \node[state,accepting]  (r) [right of=i]     {$\textbf{r}emoved$};

  \path[->]
  (s)   edge  node {infection} (i)
  (i) edge   node [align=center]{recovery/\\ death}  (r);
\end{tikzpicture}
    \caption{State diagram describing the SIR model}
\end{figure}\pause
\vspace{-.3cm}
Equations governing the number of people in each state: \footnote<2->{\textbf{s(t), i(t), r(t):} fractions of individuals in each state; \textbf{b:} average percentage of contacts of an infectious person that catch the disease; \textbf{k:} average percentage of people recovering/dying per time unit}\pause
\begin{alignat}{3}
\frac{ds}{dt} &= -b s(t) I(t) \\ % the fraction of susceptible people over time will decrease by the people that get infected in this time, this number is dependent on how many susceptible people an infectious person meets in that time span on average and how many infectious people there are
\frac{dr}{dt} &= k i(t)\\ % the fraction of the recovered people increases with the people recovered every time unit, that fraction depends on how quickly the people are no longer infectious and how many infectious people were there in the first place
\frac{di}{dt} &= b s(t) i(t) - k i(t) % the infectious fraction is increased by the number of newly infected people and decreased by the number of no longer infectious ones
\end{alignat}
\end{frame}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\begin{frame}{SIR Model by Smith and Moore (cont)}
    \begin{figure}
        \centering
        \includegraphics[width=.6\textwidth]{pictures/sir-model.png}
        \caption{Graph of a SIR model with set parameters \citep{sir-graph-article}}
        \label{fig:sir}
    \end{figure}
\end{frame}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\begin{frame}{MSEIR model \footnotesize\citep{mseir}}
    \begin{figure}
        \centering
        \begin{tikzpicture}[shorten >=1pt,node distance=2cm,on grid,auto,scale=0.8, every node/.style={transform shape}]
        \tikzstyle{every state}=[fill={rgb:black,1;white,10}]
    \node[state]   (M)                  {$M$};
    \node[label] (startM) [above of=M, node distance=1cm] {$b(N-S)$};
    \node[state]   (S)   [right of=M]   {$S$};
    \node[label] (startS) [above of=S, node distance=1cm] {$bS$};
    \node[state] (E)  [right of=S]              {$E$};
    \node[state]           (I)  [right of=E]    {$I$};
    \node[state,accepting]           (R)  [right of=I]    {$R$};

    \path[->]
    
    (M) edge [loop below] node {$bM$}    (   )
          edge node {$\delta M$}    (S)
    (S) edge [loop below]  node {$bS$}    (     )
          edge  node {$\mu S$}    (E)
    (E) edge [loop below]  node {$bE$}    (     )
          edge  node {$\epsilon E$}    (I)
    (I) edge [loop below]  node {$bI$}    (     )
          edge  node {$\gamma I$}    (R)
    (R) edge [loop below]  node {$bR$} ( )
    (startM) edge node {} (M)
    (startS) edge node {} (S);
        \end{tikzpicture}
        \vspace{-.2cm}
        \caption{\footnotesize State transitions in the MSEIR model, based on \citep{mseir}}
    \end{figure}
\vspace{-.5cm}
\begin{multicols}{2}
	\begin{itemize}\footnotesize
		\item N: the total population
		\item M: the individuals with passive immunity, protected by maternal antibodies
		\item S: the susceptible class, those individuals who can incur the disease but are not yet exposed to the disease
		\item E: the individuals exposed to the disease but not yet infected
		\item I: the individuals infected by the disease and transmitting the disease to others
		\item R: the recovered, with permanent immunity
	\end{itemize}
\end{multicols}
\end{frame}

\begin{frame}{R(0) \footnotesize\citep{notes-on-r0,r0-calculation}}
	\begin{quotation}
		``the expected number of secondary cases
produced by a single (typical) infection in a completely susceptible population''
	\flushright\citep{notes-on-r0}
	\end{quotation}\pause
	In mathematical notation:
	\begin{align*}
		R_0 = infection\_probability \cdot avg\_contact\_rate \cdot infection\_duration 
	\end{align*}\pause
	\begin{block}{Two main approaches of calculating $R_0$:}
	\begin{enumerate}
		\item on individual level: by contact tracing
		\item on population level: from cumulative incidence data
	\end{enumerate}
	\end{block}
\end{frame}

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\section{Implementation}

% maybe add slide about boid implementation

\begin{frame}{Our modified MSEIRS model}
	\include{our-mseir-model-with-hyper-params}
	\footnotesize{\textbf{Legend:}\\
	\hspace{.75cm} \textit{\textbf{\%:}} percentage of population, \textit{\textbf{p:}} period, \textit{\textbf{P:}} probability, \\
	\hspace{.75cm} \textit{\textbf{f(I):}} infection function (see next slide)}
\end{frame}

\begin{frame}{Infection function:}
    \footnotesize $min(INFECTION\_PROBABILITY\_FACTOR, \frac{INFECTION\_PROBABILITY\_FACTOR}{distance^3})$
\begin{figure}
	\centering
	\includegraphics[width=.65\textwidth]{pictures/infection-function}
	\caption{Infection function for three different values of \texttt{INFECTION\_PROBABILITY\_FACTOR}}
\end{figure}
\end{frame}

\begin{frame}[t]{Disease metrics - $R_0$}
Measure disease spread on population level with $\frac{\Delta \#susceptible}{\#infectious}$
\begin{figure}
	\centering
	\begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
    \begin{axis}[
    table/col sep=comma, 
    title={},
    xlabel=t,
    ylabel=$R_0$
    ]
	\addplot table [x=t,y=r0,mark = none] {data/state_counts_example.csv};
	\end{axis}
	\end{tikzpicture}
	\caption{A sample development of calculated $R\_0$ value during epidemic}
\end{figure}
\end{frame}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\section{Experiments}

\begin{frame}{No Social Distancing}
\movie{\includegraphics[width=\textwidth]{pictures/no-social-distancing}}{videos/no-social-distancing.webm}
\end{frame}
\begin{frame}{Social Distancing}
\movie{\includegraphics[width=\textwidth]{pictures/social-distancing}}{videos/social-distancing.webm}
\end{frame}
\begin{frame}{A deadly disease}
\movie{\includegraphics[width=\textwidth]{pictures/death-to-all-boids}}{videos/death-to-all-boids.webm}
\end{frame}

\begin{frame}
\begin{block}{Most significant hyper parameters, subjects to our experiments:}\pause
Disease parameters:
\begin{itemize}
	\item \texttt{INFECTION\_PROBABILITY\_FACTOR}\pause
	\item \texttt{AVG\_INFECTIOUS\_PERIOD}\pause
\end{itemize}
\vspace{.2cm}
Population parameters:
\begin{itemize}
	\item \texttt{SOCIAL\_DISTANCE}\pause
\end{itemize}
\end{block}

\begin{block}{Result parameters to measure effect of experiment:}
\begin{itemize}
	\item \texttt{outbreak\_duration}\pause
	\item \texttt{max\_$R_0$}\pause
	\item \texttt{remaining\_susceptible\_boids}
\end{itemize}
\end{block}
\end{frame}

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\begin{frame}{Experiment 1}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{AVG\_INFECTIOUS\_PERIOD}, 5 runs per value
	\item \texttt{MAX\_INFECTION\_PROBABILITY} = 0.2
	\item \texttt{SOCIAL\_DISTANCE} = 20
\end{itemize}
\end{small}
    \begin{figure}
        \vspace{-0.2cm}
        \centering
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Average outbreak duration},
            xmin=0, xmax=10,
            ymin=200, ymax=1100,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=TealBlue,
                mark=square,
                ]
                coordinates {
                (0,484)(2,583)(4,617)(6,916)(8,1051)(10,963)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on average outbreak duration}
    \end{figure}
\end{frame}
\begin{frame}{Experiment 1 cont.}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{AVG\_INFECTIOUS\_PERIOD}, 5 runs per value
	\item \texttt{MAX\_INFECTION\_PROBABILITY} = 0.2
	\item \texttt{SOCIAL\_DISTANCE} = 20
\end{itemize}
\end{small}
    \begin{figure}
        \vspace{-0.2cm}
        \centering
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Maximum $R_0$},
            xmin=0, xmax=10,
            ymin=0, ymax=1.3,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=Green,
                mark=square,
                ]
                coordinates {
                (0,1)(2,0.733)(4,0.3619)(6,0.3638)(8,0.3467)(10,0.2352)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on $R_0$}
    \end{figure}
\end{frame}
\begin{frame}{Experiment 1 cont.}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{AVG\_INFECTIOUS\_PERIOD}, 5 runs per value
	\item \texttt{MAX\_INFECTION\_PROBABILITY} = 0.2
	\item \texttt{SOCIAL\_DISTANCE} = 20
\end{itemize}
\end{small}
    \begin{figure}
        \centering
        \vspace{-0.2cm}
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Avg \#never infected boids},
            xmin=0, xmax=10,
            ymin=0, ymax=40,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=MidnightBlue,
                mark=square,
                ]
                coordinates {
                (0,34)(2,24.6)(4,9.2)(6,9.4)(8,11)(10,5.4)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on how many boids are spared}
    \end{figure}
\end{frame}
\begin{frame}[t]{Experiment 1 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_avg_infectious_period_0}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{AVG\_INFECTIOUS\_PERIOD} = 0}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 1 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_avg_infectious_period_2}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{AVG\_INFECTIOUS\_PERIOD} = 2}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 1 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_avg_infectious_period_4}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{AVG\_INFECTIOUS\_PERIOD} = 4}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 1 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_avg_infectious_period_6}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{AVG\_INFECTIOUS\_PERIOD} = 6}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 1 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_avg_infectious_period_8}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{AVG\_INFECTIOUS\_PERIOD} = 8}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 1 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_avg_infectious_period_10}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{AVG\_INFECTIOUS\_PERIOD} = 10}
\end{figure}
\end{frame}

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

\begin{frame}{Experiment 2}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{INFECTION\_PROBABILITY\_FACTOR}, 5 runs per value
	\item \texttt{AVG\_INFECTIOUS\_PERIOD} = 3
	\item \texttt{SOCIAL\_DISTANCE} = 20
\end{itemize}
\end{small}
    \begin{figure}
        \vspace{-0.2cm}
        \centering
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Average outbreak duration},
            xmin=0, xmax=1,
            ymin=200, ymax=1100,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=TealBlue,
                mark=square,
                ]
                coordinates {
                (0,232)(0.2,726)(0.4,663)(0.6,590)(0.8,681)(1,541)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on average outbreak duration}
    \end{figure}
\end{frame}
\begin{frame}{Experiment 2 cont.}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{INFECTION\_PROBABILITY\_FACTOR}, 5 runs per value
	\item \texttt{AVG\_INFECTIOUS\_PERIOD} = 3
	\item \texttt{SOCIAL\_DISTANCE} = 20
\end{itemize}
\end{small}
    \begin{figure}
        \vspace{-0.2cm}
        \centering
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Maximum $r(0)$},
            xmin=0, xmax=1,
            ymin=0, ymax=1.3,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=Green,
                mark=square,
                ]
                coordinates {
                (0,0)(0.2,0.5167)(0.4,0.2452)(0.6,0.4238)(0.8,0.0843)(1,0.423)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on $R_0$}
    \end{figure}
\end{frame}
\begin{frame}{Experiment 2 cont.}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{INFECTION\_PROBABILITY\_FACTOR}, 5 runs per value
	\item \texttt{AVG\_INFECTIOUS\_PERIOD} = 3
	\item \texttt{SOCIAL\_DISTANCE} = 20
\end{itemize}
\end{small}
    \begin{figure}
        \centering
        \vspace{-0.25cm}
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Avg \#never infected boids},
            xmin=0, xmax=1,
            ymin=0, ymax=40,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=MidnightBlue,
                mark=square,
                ]
                coordinates {
                (0,46.4)(0.2,16.2)(0.4,13.2)(0.6,8.6)(0.8,7.2)(1,4)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on how many boids are spared}
    \end{figure}
\end{frame}
\begin{frame}[t]{Experiment 2 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_infectious_probability_factor0}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 2 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_infectious_probability_factor0.2}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0.2}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 2 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_infectious_probability_factor0.4}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0.4}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 2 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_infectious_probability_factor0.6}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0.6}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 2 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_infectious_probability_factor0.8}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0.8}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 2 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_infectious_probability_factor1}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{INFECTION\_PROBABILITY\_FACTOR} = 1}
\end{figure}
\end{frame}

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\begin{frame}{Experiment 3}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{SOCIAL\_DISTANCE}, 5 runs per value
	\item \texttt{AVG\_INFECTIOUS\_PERIOD} = 3
	\item \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0.2
\end{itemize}
\end{small}
    \begin{figure}
        \vspace{-0.2cm}
        \centering
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Average outbreak duration},
            xmin=0, xmax=25,
            ymin=200, ymax=1100,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=TealBlue,
                mark=square,
                ]
                coordinates {
                (0,508)(5,595)(10,680)(15,785)(20,662)(25,654)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on average outbreak duration}
    \end{figure}
\end{frame}
\begin{frame}{Experiment 3 cont.}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{SOCIAL\_DISTANCE}, 5 runs per value
	\item \texttt{AVG\_INFECTIOUS\_PERIOD} = 3
	\item \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0.2
\end{itemize}
\end{small}
    \begin{figure}
        \vspace{-0.2cm}
        \centering
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Maximum $r(0)$},
            xmin=0, xmax=25,
            ymin=0, ymax=1.3,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=Green,
                mark=square,
                ]
                coordinates {
                (0,0.4278)(5,0.5167)(10,0.6133)(15,1.2571)(20,0.4067)(25,0.54)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on $R_0$} % it's spiking on 30 because the boids can't get far enough away from each other so they're on the borders which makes them close to each other again
    \end{figure}
\end{frame}
\begin{frame}{Experiment 3 cont.}
% experiment setup
% results
% animation: with \movie{\includegraphics[width=\textwidth]{ThumbnailGraphic}}{Video.webm}
\vspace{-0.15cm}
\begin{small}
\begin{itemize}
	\item Experimenting with: \texttt{SOCIAL\_DISTANCE}, 5 runs per value
	\item \texttt{AVG\_INFECTIOUS\_PERIOD} = 3
	\item \texttt{INFECTION\_PROBABILITY\_FACTOR} = 0.2
\end{itemize}
\end{small}
    \begin{figure}
        \centering
        \vspace{-0.2cm}
        \begin{tikzpicture}[scale=0.7, every node/.style={transform shape}]
            \begin{axis}[
            title={},
            xlabel={\small \texttt{AVG\_INFECTIOUS\_PERIOD}},
            ylabel={\small Avg \#never infected boids},
            xmin=0, xmax=25,
            ymin=0, ymax=40,
            xmajorgrids=true,
            ymajorgrids=true,
            grid style=dashed,
            ]

                \addplot[
                color=MidnightBlue,
                mark=square,
                ]
                coordinates {
                (0,24.4)(5,29.4)(10,18)(15,13.6)(20,23.4)(25,22.4)
                };
            \end{axis}
        \end{tikzpicture}
        \vspace{-0.5cm}
        \caption{Influence of hyper parameter on how many boids are spared}
    \end{figure}
\end{frame}
\begin{frame}[t]{Experiment 3 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_social_distance0}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{SOCIAL\_DISTANCE} = 0}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 3 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_social_distance5}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{SOCIAL\_DISTANCE} = 5}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 3 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_social_distance10}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{SOCIAL\_DISTANCE} = 10}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 3 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_social_distance15}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{SOCIAL\_DISTANCE} = 15}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 3 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_social_distance20}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{SOCIAL\_DISTANCE} = 20}
\end{figure}
\end{frame}
\begin{frame}[t]{Experiment 3 cont.}
\vspace{-1cm}
\begin{figure}
	\include{stackplot_social_distance25}
	\vspace{-.9cm}
	\caption{Course of the epidemic for \texttt{SOCIAL\_DISTANCE} = 25}
\end{figure}
\end{frame}


%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\section{Evaluation}

\begin{frame}{Problems \& Approaches - The knowledge problem}
    \begin{block}{Approach 1}
        \begin{itemize}
            \item Fuzzy Sets to describe boid membership in different states
            \item results in uncertain boid states, representing system with lack of knowledge
        \end{itemize}
    \end{block}\pause
    \begin{block}{Approach 2}\pause
        \begin{itemize}
            \item full knowledge $\Rightarrow$ We are the virus
            \item boids can only know about sickness of symptomatic boids
        \end{itemize}
    \end{block}
\end{frame}

\begin{frame}{Problems \& Approaches - Inefficiency}
\begin{block}{Problem}
Boid distance calculation mechanic is super slow
\end{block}\pause
\begin{block}{Reason}
Distances need to be computed between each boid $\Rightarrow$ $\mathcal{O}=n^2$
\end{block}\pause
\begin{block}{Possible Solution:}
\begin{itemize}
	\item use \texttt{numpy} instead of built-in python functions
	\item implement boids in a multidimensional array instead of lists of boids and their distances
\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Problems \& Approaches - Infection function}
\begin{block}{Approach 1}
\textbf{Linear} function, 100\% infection probability at minimum distance, 0\% at a set escape distance\\
\vspace{.1cm}
\textbf{Issue with that:} unrealistically high spread no matter the infection probability factor, no boid can escape infection
\end{block}\pause
\begin{block}{Approach 2}
Infection probability depends on \textbf{inverse cube} of distance, instead of probability factor now a probability maximum $\Rightarrow$ more realistic disease spread
\end{block}
\end{frame}

\begin{frame}{Problems \& Approaches - Infection function}
The old, linear infection function $y = -\frac{1}{D-M} x + 1 + \frac{1}{D-M} \cdot M$:
\vspace{-.6cm}
\begin{figure}
	\begin{tikzpicture}[scale=0.8, every node/.style={transform shape}]
    \begin{axis}[
    title={},
    xlabel={\small distance to infectious boid},
    ylabel={\small probability of infection},
    xmin=0, xmax=200,
    ymin=0, ymax=1,
    ymajorgrids=true,
    xmajorgrids=true,
    grid style=dashed,
    domain=0:200,
    legend pos=outer north east
    ]

    \addplot[MidnightBlue, ultra thick] {(-1/(200-30))*x+(1/(200-30))*30+1};
    \addplot[Plum, ultra thick] {(-1/(100-30))*x+(1/(100-30))*30+1};
    \addplot[TealBlue, ultra thick] {.75*((-1/(100-10))*x+(1/(100-10))*10+1)};
    \addplot[WildStrawberry, ultra thick] {.3*((-1/(150-0))*x+1)};
    \legend{\footnotesize{D = 200 \& P = 1.00 \& M = 30}, 
            \footnotesize{D = 100 \& P = 1.00 \& M = 30},
            \footnotesize{D = 100 \& P = 0.75 \& M = 10}, 
            \footnotesize{D = 150 \& P = 0.30 \& M = 0}}
    \end{axis}
\end{tikzpicture}
    \vspace{-.7cm}
	\caption{Infection probability depending on distance and hyper-parameters escape distance D and probability of infection P}
\end{figure}
\end{frame}

\begin{frame}{Problems \& Approaches - Flocking}
\begin{block}{Problem}
After some final adjustments: flocking not working as well as before
\end{block}\pause
\begin{block}{Reason}
\begin{itemize}
	\item too huge distance hyper parameters
	\item mistake in symptomatic avoidance algorithm
\end{itemize}
\end{block}\pause
\begin{block}{Lessons learned}
	The boids system is sensitive to hyperparameter settings % maybe formulate that in better way
\end{block}
\end{frame}

\begin{frame}{Problems \& Approaches - Social Distancing}
\begin{block}{Approach 1}
	Avoid visibly sick, symptomatic boids\\
	\vspace{0.1cm}
	\textbf{Issue:} retrieving boids in \texttt{DISEASE\_DETECTION\_DISTANCE} each iteration \& looking for symptomatic boids produces a lot of computing overhead
\end{block}\pause
\begin{block}{Approach 2}
	Avoid all boids that are in \texttt{SOCIAL\_DISTANCE} instead of only the sick boids
\end{block}
\end{frame}

\begin{frame}{Conclusions}
\begin{itemize}
	\item for our simulation, $R_0$ is not the best metric for the severity of the ``epidemic'' $\Rightarrow$ if everyone is infected, nobody can infect anyone else (see Experiment 2)
    \item too few boids and too small a space $=$ distorted simulation behavior and unreliable metrics
	\item simulation of social distancing is harder than expected
\end{itemize}
\end{frame}

\begin{frame}{Further research and improvement possibilities}
\begin{itemize}
	\item calibration of hyper parameters (not optimal yet)
	\item developing heuristics for evaluating simulation quality
	\item find better metric or better metric calculation to measure disease attributes
    \item a more efficient implementation would allow for a larger sample size and therefore more realistic simulation results
\end{itemize}
\end{frame}

\begin{frame}[allowframebreaks]{References}
	\bibliography{../Research/literature}
	% \nocite{*} % uncomment in case we want to show all our research material
    \bibliographystyle{plainnat}
\end{frame}

\section*{}
\begin{frame}{The end}
    \centering\Large
    Thank you for listening!\\
    \vspace{1cm}
    Any questions?
\end{frame}

\end{document}