from enum import Enum
from hyper_params.population import DEFAULT_SPEED
import pygame as pg

class BoidState(Enum):
	MATERNALLY_IMMUNE = {"image": pg.image.load("sprites/maternally-immune-boid.png")}
	SUSCEPTIBLE = {"image": pg.image.load("sprites/susceptible-boid.png")}
	EXPOSED = {"image": pg.image.load("sprites/exposed-boid.png")}
	INFECTIOUS = {"image": pg.image.load("sprites/infectious-boid.png")}
	RECOVERED = {"image": pg.image.load("sprites/recovered-boid.png")}
