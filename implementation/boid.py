import pygame as pg
import random
import numpy as np

from numpy import mean, math
from numpy.ma import arctan2, cos, sin
from math import pi

from boid_state import BoidState
from hyper_params.population import *
from hyper_params.disease import *
from hyper_params.misc import *
from clock import Clock

class Boid():
	image = NotImplemented
	state_change_time = 0
	symptomatic = False
	symptoms_begin = NotImplemented
	@property
	def state(self):
		return self._state

	def __init__(self, x, y, direction, id, clock, infection_function, INFECTIOUS_PERIOD_PARAMS, MAX_INFECTIOUS_PROBABILITY):
		self.x = x
		self.y = y
		self.direction = direction
		self.id = id
		self.dead = False
		self.clock = clock
		self.state = self.calc_initial_state()
		self.set_symptomatic()
		self.INCUBATION_TIME = calc_random_period(INCUBATION_TIME_PARAMS)
		self.INFECTIOUS_PERIOD = calc_random_period(INFECTIOUS_PERIOD_PARAMS)
		self.SYMPTOMATIC_PERIOD = calc_random_period(SYMPTOMATIC_PERIOD_PARAMS)
		self.IMMUNITY_PERIOD = calc_random_period(IMMUNITY_PERIOD_PARAMS)
		self.MATERNAL_IMMUNE_PROTECTION_PERIOD = calc_random_period(MATERNAL_IMMUNE_PROTECTION_PERIOD_PARAMS)
		self.calc_infection_probability = infection_function
		self.max_infectious_probability = MAX_INFECTIOUS_PROBABILITY

	def calc_initial_state(self):
		return random.choices(list(BoidState), weights=INITIAL_STATE_DISTRIBUTION, k=1)[0]

	def set_symptomatic(self):
		if self.state == BoidState.INFECTIOUS:
			self.symptomatic = random.choices([True, False], weights=(LIKELIHOOD_SYMPTOMATIC, 1-LIKELIHOOD_SYMPTOMATIC), k=1)[0]
		if self.symptomatic and self.symptoms_begin == NotImplemented:
			self.symptoms_begin = self.clock.time_ticks

	def set_image(self):
		if self.symptomatic:
			if self.state == BoidState.INFECTIOUS:
				self.image = pg.transform.rotozoom(INFECTIOUS_SYMPTOMATIC_IMG, self.direction, BOID_SCALE)
			elif self.state == BoidState.RECOVERED:
				self.image = pg.transform.rotozoom(RECOVERED_SYMPTOMATIC_IMG, self.direction, BOID_SCALE)
		else:
			self.image = pg.transform.rotozoom(self.state.value["image"], self.direction, BOID_SCALE)

	def get_close_boids(self, i, distances, boids, max_distance):
		close = []
		close_seen = []
		for j in range(len(boids)):
			if j != i and distances[i, j] <= max_distance:
				close.append((i, j))
		for _, j in close:
			direction_delta = arctan2(self.y - boids[j].y, self.x - boids[j].x) * 180 / pi + 180
			if direction_delta >= self.direction - ANGLE_OF_DETECTION/2 or direction_delta < self.direction + ANGLE_OF_DETECTION/2:
				close_seen.append(boids[j])
		return close_seen

	def get_center(self, boids):
		# Returns common center, returns (nan, nan) if there's no close boids.
		X, Y = [],[]
		if len(boids) == 0:
			return self.x, self.y

		for b in boids:
			X.append(b.x)
			Y.append(b.y)
		return mean(X), mean(Y)

	def direction_to_point(self, point):
		x, y = point
		# returns if one of the numbers is nan
		if x != x or y != y:
			return self.direction
		elif x != self.x and y != self.y:
			return arctan2(self.y - y, self.x - x) * 180 / pi + 180
		else:
			return self.direction

	def weighted_direction(self, center_angle, avoid_angle, mean_angle):
		x = 0
		y = 0
		for a, w in zip([center_angle, avoid_angle, mean_angle], [COHESION_WEIGHT, SEPARATION_WEIGHT, ALIGNMENT_WEIGHT]):
			x += cos(math.radians(a)) * w
			y += sin(math.radians(a)) * w
		return math.degrees(math.atan2(y, x))

	def set_new_direction(self, center_angle, avoid_angle, nearby):
		# get orientation mean
		if len(nearby) == 0:
			mean_angle = self.direction
		else:
			angle1 = np.sum(np.cos([i.direction * np.pi / 180 for i in nearby]))
			angle2 = np.sum(np.sin([i.direction * np.pi / 180 for i in nearby]))
			mean_angle = np.arctan2(angle2, angle1) * 180 / np.pi
			if mean_angle == 0:
				mean_angle = self.direction
			elif mean_angle < 0:
				mean_angle += 360

		# put it all together to get and set the angle for the boid
		new_direction = self.weighted_direction(center_angle, avoid_angle, mean_angle)
		self.direction = self.turning_angle(new_direction)

	def turning_angle(self, direction):
		theta = (self.direction - direction + 180) % 360 - 180
		if theta < 0:
			return (self.direction + MAX_TURNING_ANGLE / FRAME_RATE) % 360
		elif theta > 0:
			phi = self.direction - MAX_TURNING_ANGLE / FRAME_RATE
			if phi < 0:
				return theta + 360
			else:
				return phi
		else:
			return self.direction

	@state.setter
	def state(self, new_state:BoidState):
		self._state = new_state
		self.state_change_time = self.clock.time_ticks
		self.set_image()

	def update_state(self, close_boids):
		if self.state == BoidState.MATERNALLY_IMMUNE:
			if self.clock.time_ticks - self.state_change_time > self.MATERNAL_IMMUNE_PROTECTION_PERIOD:
				self.state = BoidState.SUSCEPTIBLE
			else:
				return

		elif self.state == BoidState.SUSCEPTIBLE:
			for boid in close_boids:
				if boid.state == BoidState.INFECTIOUS:
					infection_probability = self.calc_infection_probability(self.calc_distance(boid), self.max_infectious_probability)
					self.state = random.choices([BoidState.EXPOSED, BoidState.SUSCEPTIBLE],
									weights=(infection_probability, 1-infection_probability), k=1)[0]
					if self.state == BoidState.EXPOSED: # if boid already gets infected we can stop checking
						break

		elif self.state == BoidState.EXPOSED:
			if self.clock.time_ticks - self.state_change_time > self.INCUBATION_TIME:
				self.state = BoidState.INFECTIOUS
				self.set_symptomatic()
			else:
				return

		elif self.state == BoidState.INFECTIOUS:
			if self.clock.time_ticks - self.state_change_time > self.INFECTIOUS_PERIOD:
				if random.choices([True, False], weights=(MORTALITY_PROBABILITY, 1-MORTALITY_PROBABILITY), k=1)[0]:
					self.dead = True
				else:
					self.state = BoidState.RECOVERED
			else:
				return

		elif self.state == BoidState.RECOVERED:
			if self.clock.time_ticks - self.state_change_time > self.IMMUNITY_PERIOD:
				self.state = BoidState.SUSCEPTIBLE
			else:
				return

		if self.symptomatic and self.clock.time_ticks - self.symptoms_begin > self.SYMPTOMATIC_PERIOD:
			self.symptomatic = False

	def calc_distance(self, other_boid):
		return np.sqrt(((self.x - other_boid.x)/BOID_SIZE/BOID_SCALE) ** 2 + ((self.y - other_boid.y)/BOID_SIZE/BOID_SCALE) ** 2)
