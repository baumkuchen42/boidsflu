from math import sin, cos, pi

import pygame as pg
import time
import random
import numpy as np
import csv
import os.path
import sys

from multiprocessing import Process

from boid import Boid
from boid_state import BoidState
from state_counter import StateCounter
from plot import Plot
from hyper_params.population import *
from hyper_params.misc import *
from hyper_params.disease import *
from clock import Clock

def calc_infection_probability(distance, limit_probability):
	'''
	inverse square function with 100% infection at too close distance and 0% at no infection distance
	adjusted with an infection probability factor to account for exposure time
	'''
	probability = limit_probability / distance / distance / distance
	return min(limit_probability, probability)

class Main():
	def __init__(self, run_start_time):
		self.run_start_time = run_start_time
		self.evaluation_done = False
		self.screen = pg.display.set_mode(SCREEN_SIZE)
		self.state_counter = StateCounter(self.run_start_time)
		self.boids = []

		# experiment hyper params
		self.MAX_INFECTIOUS_PROBABILITY = 0.2
		self.SOCIAL_DISTANCE = 20
		self.INFECTIOUS_PERIOD_PARAMS = [1, 10, 0.01, 1] # min, max, avg, stddev

	def init_routine(self):
		self.init_scene()
		self.clock = Clock(FRAME_RATE)
		self.init_boids(NR_BOIDS)
		pg.display.update()

	def init_scene(self):
		pg.init()
		pg.display.set_caption("BoidFlu")
		self.screen.fill((0, 0, 0))

	def init_boids(self, nr_of_boids):
		for i in range(nr_of_boids):
			new_boid = Boid(
				random.randint(0, SCREEN_SIZE[0]), random.randint(0, SCREEN_SIZE[1]),
				random.randint(0, 360),
				i, self.clock,
				calc_infection_probability,
				self.INFECTIOUS_PERIOD_PARAMS,
				self.MAX_INFECTIOUS_PROBABILITY
				)
			self.boids.append(new_boid)
			self.draw_boid(new_boid)
			self.state_counter.increase_count(new_boid.state)
		self.state_counter.write_counts_to_file(self.clock)

	def draw_boid(self, boid):
		self.screen.blit(boid.image, (boid.x, boid.y))

	def update_boids(self):
		self.distances = self.get_distances()
		for i in range(len(self.boids)):
			close_boids = self.boids[i].get_close_boids(i, self.distances, self.boids, FLOCK_DETECTION_DISTANCE)
			too_nearby = self.boids[i].get_close_boids(i, self.distances, self.boids, self.SOCIAL_DISTANCE)
			self.center_boid(close_boids, too_nearby, self.boids[i])
			self.move_boid(self.boids[i])
			self.wrap_boid(self.boids[i])
			self.update_boid_state(close_boids, self.boids[i])
		for boid in self.boids:
			if boid.dead:
				self.boids.remove(boid)
				self.state_counter.decrease_count(boid.state)
				self.state_counter.increase_dead()
				if not STATE_COUNT_COLLECTION_ONCE_PER_CYCLE:
					self.state_counter.write_counts_to_file(self.clock)
		if STATE_COUNT_COLLECTION_ONCE_PER_CYCLE:
			self.state_counter.write_counts_to_file(self.clock)
		self.update_screen() # TODO: comment out to record data without showing updates to the screen

	def get_distances(self):
		distances = {}
		for i in range(len(self.boids)):
			for j in range(i + 1, len(self.boids)):
				d = np.sqrt(
					((self.boids[i].x - self.boids[j].x)/BOID_SIZE/BOID_SCALE) ** 2 +
					((self.boids[i].y - self.boids[j].y)/BOID_SIZE/BOID_SCALE) ** 2)
				distances[j, i] = distances[i, j] = d
		return distances

	def center_boid(self, close_boids, too_nearby, boid):
		center = boid.get_center(close_boids)
		avoid_center = boid.get_center(too_nearby)

		center_angle = boid.direction_to_point(center)
		avoid_angle = boid.direction_to_point(avoid_center)

		boid.set_new_direction(center_angle, avoid_angle + 180, close_boids)
		boid.set_image()

	def move_boid(self, boid):
		speed_coefficient = SLOWING_EFFECT if boid.symptomatic else 1
		boid.x += speed_coefficient * DEFAULT_SPEED / FRAME_RATE * cos(-boid.direction * pi / 180)
		boid.y += speed_coefficient * DEFAULT_SPEED / FRAME_RATE * sin(-boid.direction * pi / 180)

	def wrap_boid(self, boid):
		X, Y = SCREEN_SIZE
		if boid.x > X:
			boid.x = 0
		elif boid.x < 0:
			boid.x = X
		if boid.y > Y:
			boid.y = 0
		elif boid.y < 0:
			boid.y = Y

	def update_boid_state(self, close_boids, boid:Boid):
		old_state = boid.state
		self.state_counter.decrease_count(old_state)
		boid.update_state(close_boids)
		self.state_counter.increase_count(boid.state)
		if not STATE_COUNT_COLLECTION_ONCE_PER_CYCLE:
			self.state_counter.write_counts_to_file(self.clock)

	def update_screen(self):
		self.screen.fill((0, 0, 0))
		for boid in self.boids:
			self.draw_boid(boid)
		pg.display.update()

	def disease_outbreak_over(self):
		return self.state_counter.state_dict['exposed']['count'] == 0 and self.state_counter.state_dict['infectious']['count'] == 0

	def evaluation(self):
		if not os.path.isfile(RESULTS_FILE):
			self.init_result_file()
		data = np.genfromtxt(STATE_COUNTS_FILE_BASE % {'timestamp': self.run_start_time}, delimiter = ',', names = True)
		with open(RESULTS_FILE, 'a') as results_file:
			writer = csv.writer(results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			writer.writerow([
				self.clock.time_ticks, max(data['r0']), self.state_counter.state_dict['susceptible']['count'], data['dead'][len(data)-1],
				INITIAL_STATE_DISTRIBUTION, NR_BOIDS, self.MAX_INFECTIOUS_PROBABILITY,
				INCUBATION_TIME_PARAMS[2], self.INFECTIOUS_PERIOD_PARAMS[2],
				LIKELIHOOD_SYMPTOMATIC, IMMUNITY_PERIOD_PARAMS[2],
				MATERNAL_IMMUNE_PROTECTION_PERIOD_PARAMS[2], self.SOCIAL_DISTANCE
				])

	def init_result_file(self):
		with open(RESULTS_FILE, 'w') as results_file:
			writer = csv.writer(results_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			writer.writerow([
				'outbreak_duration', 'max_r0', 'remaining_susc_boids', 'nr_deaths', 'INITIAL_STATE_DISTRIBUTION',
				'NR_BOIDS', 'INFECTION_PROBABILITY_FACTOR',
				'AVG_INCUBATION_TIME', 'AVG_INFECTIOUS_PERIOD', 'LIKELIHOOD_SYMPTOMATIC',
				'AVG_IMMUNITY_PERIOD', 'AVG_MATERNAL_IMMUNE_PROTECTION_PERIOD', 'SOCIAL_DISTANCE'
				])


if __name__ == '__main__':
	cmd_line_args = sys.argv[1:] # 1: MAX_INFECTIOUS_PROBABILITY, 2: SOCIAL_DISTANCE, 3: AVG_INFECTIOUS_PERIOD
	run_start_time = time.time()
	main = Main(run_start_time)
	if cmd_line_args:
		main.MAX_INFECTIOUS_PROBABILITY = float(cmd_line_args[0])
		main.SOCIAL_DISTANCE = int(cmd_line_args[1])
		main.INFECTIOUS_PERIOD_PARAMS[2] = int(cmd_line_args[2])
	main.init_routine()
	plot = Plot(run_start_time)
	Process(target=plot.start, daemon=True).start()
	running = True
	is_first = True
	while running:
		try:
			cycle_time_start = time.time()
			if main.disease_outbreak_over() and not main.evaluation_done:
				main.evaluation()
				# running = False # TODO: uncomment this line to stop the simulation when there are no more infectious boids
				main.evaluation_done = True
				# plot.stop_animation()
			for event in pg.event.get():
				if event.type == pg.QUIT or (event.type == pg.KEYDOWN and event.key == ord('q')):
					running = False

			main.clock.clock_tick() # update the clock
			main.update_boids()

			sleep_time = 1/FRAME_RATE + cycle_time_start - time.time()
			# sleep_time = 0 # TODO: Remove if you want to record or view screen
			if sleep_time < 0:
				pass #print("dropped frame")
			else:
				time.sleep(sleep_time)
			if is_first:
				#time.sleep(10)
				is_first = False
		except KeyboardInterrupt:
			running = False
# TODO: fix the R0 calculation code so that it detects invalid values (which we think indicate the end of the simulation)
