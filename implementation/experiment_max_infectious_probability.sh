#!/bin/bash
for i in {1..5}
do
	python main.py 0.0 20 3
done

echo '2nd value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.2 20 3
done

echo '3rd value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.4 20 3
done

echo '4th value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.6 20 3
done

echo '5th value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.8 20 3
done

echo '6th value' >> data/results.csv
for i in {1..5}
do
	python main.py 1 20 3
done
