class Clock:
    def __init__(self, ticks_per_second, start_time = 0):
        self.__time = start_time
        self.__ticks_per_second = ticks_per_second

    def clock_tick(self):
        self.__time += 1

    @property
    def time_seconds(self):
        return self.__time/self.__ticks_per_second

    @property
    def time_ticks(self):
        return self.__time
