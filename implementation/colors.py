from enum import Enum

class Color(Enum):
	PURPLE = '#8e03a3'
	LIGHT_PURPLE = '#dc85e9'
	BLUE = '#0369a3'
	LIGHT_BLUE = '#729fcf'
	YELLOW = '#e9b913'
	LIGHT_YELLOW = '#fde9a9'
	RED = '#ff0000'
	LIGHT_RED = '#f37d7d'
	GREEN = '#18a303'
	LIGHT_GREEN = '#92e285'
	ORANGE = '#fc5c00'
	LIGHT_ORANGE = '#f09e6f'
	GREY = '#9f9f9f'
