#!/bin/bash
for i in {1..5}
do
	python main.py 0.2 0 3
done

echo '2nd value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.2 5 3
done

echo '3rd value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.2 10 3
done

echo '4th value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.2 15 3
done

echo '5th value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.2 20 3
done

echo '6th value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.2 25 3
done

echo '7th value' >> data/results.csv
for i in {1..5}
do
	python main.py 0.2 30 3
done
