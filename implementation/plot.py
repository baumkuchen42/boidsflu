import datetime as dt
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import numpy as np
import csv
import seaborn as sns

from colors import Color
from hyper_params.misc import STATE_COUNTS_FILE_BASE, PLOT_OUTPUT_TO_FILE, PLOT_FILE

class Plot():
	def __init__(self, run_start_time):
		self.run_start_time = run_start_time

	def start(self):
		self.configure_style()
		# Create figure for plotting
		self.fig, self.ax = plt.subplots(2, figsize=(10, 5))
		self.fig.canvas.set_window_title('Development of disease numbers')
		# Set up plot to call animate() function periodically
		self.animation = animation.FuncAnimation(self.fig, self.animate, interval=10, init_func=self.init_plot)
		if PLOT_OUTPUT_TO_FILE:
			Writer = animation.writers['ffmpeg']
			writer = Writer(fps=10, metadata=dict(artist='Me'), bitrate=1800)
			self.animation.save(PLOT_FILE, writer=writer)
		plt.ylabel('Number of boids in state')
		plt.xlabel('t')
		plt.show()

	def stop_animation(animation):
		# doesn't work because animation is not defined yet when this method is defined due to the multithreading nature of the calling of plot
		animation.event_source.stop()

	def animate(self, i):
		self.clear_plots()
		self.create_plots()

	def init_plot(self):
		self.add_color_labels()
		self.fig.legend(loc=7)
		self.ax[0].set_ylabel('Number of boids in state')
		self.ax[1].set_ylabel('R(0)')
		self.ax[0].set_xlabel('t')
		self.fig.subplots_adjust(right=0.75, hspace=.5)

	def add_color_labels(self):
		self.ax[0].plot([], [], Color.PURPLE.value, label='maternally immune', linewidth=3)
		self.ax[0].plot([], [], Color.BLUE.value, label='susceptible', linewidth=3)
		self.ax[0].plot([], [], Color.YELLOW.value, label='exposed', linewidth=3)
		self.ax[0].plot([], [], Color.RED.value, label='infectious', linewidth=3)
		self.ax[0].plot([], [], Color.GREEN.value, label='recovered', linewidth=3)
		self.ax[0].plot([], [], Color.GREY.value, label='dead', linewidth=3)

	def clear_plots(self):
		del self.ax[0].collections[:]

	def create_plots(self, transparancy_level:str='aa'):
		data = np.genfromtxt(STATE_COUNTS_FILE_BASE % {'timestamp': self.run_start_time}, delimiter = ',', names = True)
		stackplot = self.ax[0].stackplot(
			data['t'], data['maternally_immune'], data['susceptible'], data['exposed'], data['infectious'], data['recovered'], data['dead'],
			colors=[
				Color.PURPLE.value+transparancy_level, Color.BLUE.value+transparancy_level, Color.YELLOW.value+transparancy_level,
				Color.RED.value+transparancy_level, Color.GREEN.value+transparancy_level, Color.GREY.value+transparancy_level],
			edgecolor='black'
			)
		r0_plot = self.ax[1].plot(data['t'], data['r0'], 'lime', label='R(0)', linewidth=2)

	def configure_style(self):
		matplotlib.rcParams['toolbar'] = 'None'
		sns.set(font='Franklin Gothic Book',
			rc={
			 'axes.axisbelow': False,
			 'axes.edgecolor': 'lightgrey',
			 'axes.facecolor': 'None',
			 'axes.grid': False,
			 'axes.labelcolor': 'dimgrey',
			 'axes.spines.right': False,
			 'axes.spines.top': False,
			 'figure.facecolor': 'white',
			 'lines.solid_capstyle': 'round',
			 'patch.edgecolor': 'w',
			 'patch.force_edgecolor': True,
			 'text.color': 'dimgrey',
			 'xtick.bottom': False,
			 'xtick.color': 'dimgrey',
			 'xtick.direction': 'out',
			 'xtick.top': False,
			 'ytick.color': 'dimgrey',
			 'ytick.direction': 'out',
			 'ytick.left': False,
			 'ytick.right': False})
		sns.set_context("notebook", rc={"font.size":16,
			"axes.titlesize":20,
			"axes.labelsize":18})
		plt.style.use(['fivethirtyeight', 'dark_background'])

