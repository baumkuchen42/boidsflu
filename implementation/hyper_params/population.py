'''Hyper parameters that describe properties of the given boid population'''
# INITIAL_STATE_DISTRIBUTION = [.001, .98, .009, .006, .004] # realistic
# INITIAL_STATE_DISTRIBUTION = [.20, .20, .20, .20, .20] # for testing
INITIAL_STATE_DISTRIBUTION = [.01, .8, .1, .08, .01] # maternally immune, susceptible, exposed, infectious, recovered
NR_BOIDS = 60
FLOCK_DETECTION_DISTANCE = 40
ANGLE_OF_DETECTION = 200
DEFAULT_SPEED = 170
MAX_TURNING_ANGLE = 120
SLOWING_EFFECT = 0.95
SEPARATION_WEIGHT = 1.4
COHESION_WEIGHT = 1
ALIGNMENT_WEIGHT = 1.4
