import scipy.stats as stats
from hyper_params.misc import FRAME_RATE, SCREEN_SIZE

def calc_random_period(args):
	#print(args)
	min, max, mu, sigma = map(lambda x: x * FRAME_RATE, args) # multiply them all by framerate
	dist = stats.truncnorm((min - mu) / sigma, (max - mu) / sigma, loc=mu, scale=sigma) # generate the distribution
	return int(dist.rvs(1)[0]) # pick one number out of it

'''Hyper parameters that describe properties of the disease itself'''
INCUBATION_TIME_PARAMS = (0, 4, 3, 2)
LIKELIHOOD_SYMPTOMATIC = .75
SYMPTOMATIC_PERIOD_PARAMS = (0, 20, 4, 10) # can be less, equal or even more than infectious period
IMMUNITY_PERIOD_PARAMS = (900, 500000, 25000, 1000) # could also be forever
MATERNAL_IMMUNE_PROTECTION_PERIOD_PARAMS = (20, 500, 120, 50)
MORTALITY_PROBABILITY = 0.2
NO_INFECT_DIST = min(SCREEN_SIZE[0], SCREEN_SIZE[1])/2 # from this distance on it's impossible to get infected
