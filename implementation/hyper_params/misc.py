import pygame as pg
import time

'''Hyper parameters that don't really belong into a category, e.g. GUI parameters'''
BOID_SIZE = 909
BOID_SCALE = .01
SCREEN_SIZE = (800, 900)
FRAME_RATE = 30
INFECTIOUS_SYMPTOMATIC_IMG = pg.image.load("sprites/infectious-boid_symptomatic.png")
RECOVERED_SYMPTOMATIC_IMG = pg.image.load("sprites/recovered-boid_symptomatic.png")
STATE_COUNTS_FILE_BASE = 'data/state_counts_%(timestamp)s.csv'
PLOT_OUTPUT_TO_FILE = False # if True, no plot is shown but the plot is directly written to file
PLOT_FILE = f'data/plot_{time.time()}.mp4'
STATE_COUNT_COLLECTION_ONCE_PER_CYCLE = True # should be False for fluent plot and True for data collection
RESULTS_FILE = f'data/results.csv'
