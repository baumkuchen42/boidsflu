import csv
import numpy as np

from boid_state import BoidState
from hyper_params.misc import STATE_COUNTS_FILE_BASE


class StateCounter():
	def __init__(self, run_start_time):
		self.run_start_time = run_start_time
		self.state_dict = {
			'maternally_immune': {'state_name': BoidState.MATERNALLY_IMMUNE, 'count': 0,
								  'path': 'data/maternally_immune'},
			'susceptible': {'state_name': BoidState.SUSCEPTIBLE, 'count': 0, 'path': 'data/susceptible'},
			'exposed': {'state_name': BoidState.EXPOSED, 'count': 0, 'path': 'data/exposed'},
			'infectious': {'state_name': BoidState.INFECTIOUS, 'count': 0, 'path': 'data/infectious'},
			'recovered': {'state_name': BoidState.RECOVERED, 'count': 0, 'path': 'data/recovered'},
			'dead': {'state_name': None, 'count': 0, 'path': 'data/dead'}}
		self.init_files()

	def init_files(self):
		with open(STATE_COUNTS_FILE_BASE % {'timestamp': self.run_start_time}, 'w') as data_file:
			writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			writer.writerow(
				["t", "maternally immune", "susceptible", "exposed", "infectious", "recovered", "dead", "r0"])

	def increase_count(self, state: BoidState):
		for attr, value in self.state_dict.items():
			if value['state_name'] == state:
				value['count'] += 1

	def decrease_count(self, state: BoidState):
		for attr, value in self.state_dict.items():
			if value['state_name'] == state:
				value['count'] -= 1

	def write_counts_to_file(self, clock):
		with open(STATE_COUNTS_FILE_BASE % {'timestamp': self.run_start_time}, 'a') as data_file:
			writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			writer.writerow(
				[clock.time_ticks] + [value['count'] for attr, value in self.state_dict.items()] + [self.calc_r0()])

	def increase_dead(self):
		self.state_dict['dead']['count'] += 1

	def calc_r0(self):
		data_length = len(open(STATE_COUNTS_FILE_BASE % {'timestamp': self.run_start_time}).readlines(  ))
		if data_length == 1:
			return 0
		data = np.genfromtxt(STATE_COUNTS_FILE_BASE % {'timestamp': self.run_start_time}, delimiter=',', names=True)
		# print(data)
		if data['susceptible'].size < 2 or data['infectious'][len(data) - 1] == 0:
			return 0
		return (data['susceptible'][len(data) - 2] - data['susceptible'][len(data) - 1]) / data['infectious'][len(data) - 1]

