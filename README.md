# Boid Flu

This project implements a basic boids simulation with an epidemiological extension that allows for the modeling of contagious disease.

Details about the project can be found in the file Finalpresentation.pdf.
Data from the experiments that we ran can be found in the data directory.

# How to run

1. Go to `implementation/`

2. Install modules from `requirements.txt`.

3. Navigate to the location of `main.py` in your shell

4. Execute the command below.  Note that the simulation has default parameters that should provide a nice demonstration.
```sh
python main.py [MAX_INFECTIOUS_PROBABILITY SOCIAL_DISTANCE INFECTIOUS_PERIOD_PARAMS]
```

# Troubleshooting

## Videos not playing in the presentation

The videos are not compiled into the presentation, instead they stored in `final-presentation/videos` and accessed by the PDF viewer. Not all PDF viewers are capable of playing those embedded videos, known capable PDF viewers are:

- Adobe Reader
- Evince
