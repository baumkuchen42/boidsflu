from experimenting.fuzzy_stuff.fuzzy_boolean import FuzzyBool


class FuzzySet():
	'''
	A class to implement a fuzzy set based on two lists,
	the list of certain members and the list of uncertain ones.
	Also contains some basic operations to make accessing this class
	independent from its underlying datastructure for certains and uncertains.
	'''

	def __init__(self):
		self.certain_members = []
		self.uncertain_members = []

	def add_uncertain(self, item):
		self.uncertain_members.append(item)

	def add_certain(self, item):
		self.certain_members.append(item)

	def remove_uncertain(self, item):
		self.uncertain_members.remove(item)

	def remove_certain(self, item):
		self.certain_members.remove(item)

	def remove(self, item):
		if item in self.certain_members:
			self.certain_members.remove(item)
		elif item in self.uncertain_members:
			self.uncertain_members.remove(item)

	def move_to_certain(self, item):
		self.remove_uncertain(self, item)
		self.add_certain(self, item)

	def is_member(self, item):
		if item in self.certain_members:
			return FuzzyBool.TRUE
		elif item in self.uncertain_members:
			return FuzzyBool.MAYBE
		else:
			return FuzzyBool.FALSE

	def get_certain_members(self):
		return self.certain_members

	def get_uncertain_members(self):
		return self.uncertain_members