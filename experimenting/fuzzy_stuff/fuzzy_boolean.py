from enum import Enum
class FuzzyBool(Enum):
	TRUE = 1
	FALSE = 0
	MAYBE = .5
