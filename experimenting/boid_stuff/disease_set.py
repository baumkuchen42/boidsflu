from experimenting.boid_stuff.colors import Color
from experimenting.fuzzy_stuff.fuzzy_set import FuzzySet


class DiseaseSet(FuzzySet):
	def __init__(self, name:str, certain_color:Color, uncertain_color):
		super().__init__()
		self.name = name
		self.certain_color = certain_color
		self.uncertain_color = uncertain_color
