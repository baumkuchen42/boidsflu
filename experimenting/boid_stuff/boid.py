from experimenting.boid_stuff.colors import Color
from experimenting.boid_stuff.set_database import SetDatabase, get_set_by_name
from experimenting.fuzzy_stuff.fuzzy_boolean import FuzzyBool
from experimenting.fuzzy_stuff.fuzzy_set import FuzzySet


class Boid():
	state = []

	def __init__(self):
		self.set_initial_state()

	def set_initial_state(self):
		# TODO: randomly choose from possible initial states, with different weight
		pass

	def get_state(self):
		state = []
		for set in SetDatabase:
			if set.is_member(self).value > 0:
				state.append((set.name, set.is_member(self).value))

	def set_state(self, new_set:FuzzySet, certainty:FuzzyBool):
		if certainty.value == 1:
			for set in SetDatabase:
				if set.is_member(self).value > 0:
					set.remove(self)
			new_set.add_certain(self)
			self.state.clear()
		else:
			new_set.add_uncertain(self)
		self.state.append((new_set.name, certainty))
		self.react_to_state_change()

	def react_to_state_change(self):
		if len(self.state) > 1:
			self.set_color(None, None)
		else:
			self.set_color(self.state[0][0], self.state[0][0])

	def set_color(self, set_name:str, certainty:FuzzyBool):
		# TODO find out how to change svg fill attribute via python code
		color = ''
		if set_name is None:
			color = Color.GREY
		else:
			if certainty == 1:
				color = get_set_by_name(set_name).certain_color
			else:
				color = get_set_by_name(set_name).uncertain_color
		# now set color of boid svg
