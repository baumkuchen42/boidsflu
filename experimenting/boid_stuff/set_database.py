from experimenting.boid_stuff.colors import Color
from experimenting.boid_stuff.disease_set import DiseaseSet

maternally_immune = DiseaseSet('maternally_immune', Color.PURPLE, Color.LIGHT_PURPLE)
susceptible = DiseaseSet('susceptible', Color.BLUE, Color.LIGHT_BLUE)
exposed = DiseaseSet('exposed', Color.YELLOW, Color.LIGHT_YELLOW)
infectious = DiseaseSet('infectious', Color.RED, Color.LIGHT_RED)
removed = DiseaseSet('removed', Color.GREEN, Color.LIGHT_GREEN)
symptomatic = DiseaseSet('symptomatic', Color.ORANGE, Color.LIGHT_ORANGE)

SetDatabase = [maternally_immune, susceptible, exposed, infectious, removed, symptomatic]

def get_set_by_name(name:str):
	for set in SetDatabase:
		if set.name == name:
			return set
